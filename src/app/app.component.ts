import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck, OnInit {
  title = 'Curso de Angular 4 Avanzado';
  emailContacto: string;

  ngDoCheck(){
  //	console.log("Metodo on DoCheck lanzado.");
    this.emailContacto = localStorage.getItem('emailContacto');
  }
  
  ngOnInit(){
    this.emailContacto = localStorage.getItem('emailContacto');
    console.log('Guardado: ', localStorage.getItem('emailContacto'));
  }

  borrarEmail(){
    localStorage.removeItem('emailContacto');
    localStorage.clear();
    this.emailContacto = null;
  }
}
