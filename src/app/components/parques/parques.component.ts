import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit, OnDestroy } from '@angular/core';

@Component({
	selector: 'parques',
	templateUrl: './parques.component.html',
	styleUrls:  ['./parques.component.css']
})
export class ParquesComponent implements OnChanges, OnInit, OnDestroy {
	
  @Input() nombre: string;
  @Input('metros_cuadrados') metros: number;
  public vegetacion: string;		
  public abierto: boolean;
  
  @Output() pasameLosDatos = new  EventEmitter(); 

  constructor(){
		this.nombre 	= 'Parque natural para caballos';
		this.metros		= 450;
		this.vegetacion = 'Alta';
		this.abierto	= true;
	}

	ngOnChanges(changes: SimpleChanges){
		// 1º en ejecutarse y al cambiar una propiedad		
		//console.log(changes);
		console.log('Exiten cambios en las propiedades');
	}

	ngOnInit(){
		console.log("Metodo on init lanzado.");
	}

	ngOnDestroy(){
		console.log("Metodo on ngOnDestroy lanzado.");
	}

	emitirEvento(){
		this.pasameLosDatos.emit({
			"nombre":  this.nombre,
			"metros": this.metros,
			"vegetacion": this.vegetacion,
			"abierto": this.abierto,
		});			
	}
}
