import { Component } from '@angular/core';

@Component({
	selector: 'tienda',
	templateUrl: './tienda.component.html',
	styleUrls:  ['./tienda.component.css']
})
export class TiendaComponent {
	
  public titulo;
  nombreDelParque: string;	
  public miparque;

  constructor(){
		this.titulo = 'Esta es la tienda';
	}

	mostrarNombre() {
		console.log(this.nombreDelParque);
	}

	verDatosParque(event){
		console.log(event);
		this.miparque = event;
	}
}